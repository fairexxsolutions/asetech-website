const nodemailer = require("nodemailer");

const mailerFunc = async (toMail,title,body,receipient) => {

  const transporter = nodemailer.createTransport({
   pool: true,
    service: process.env.EMAIL_SERVICE,
    auth: {
      user: process.env.EMAIL_USERNAME,
      pass: process.env.EMAIL_PASSWORD,
    },
    tls: {
      rejectUnauthorized: false
    }
  });

  let info = await transporter.sendMail({
    from: process.env.FROM_EMAIL,
    to: toMail,
    subject: title,
    html: `
    ${body}
    <div style="
    border-spacing: 0;
    border-collapse: collapse;
    color: #222;
    direction: ltr;
    font: small/1.5 Arial,Helvetica,sans-serif;
    font-family: 'Roboto',sans-serif;
    font-size: 14px;
    display: flex;
    margin-top: 30px;
    ">
    <div>
    <img 
    src="https://res.cloudinary.com/dcdf9dok3/image/upload/v1646828075/Fairexx%20and%20Asetech%20Iimages/asetechlogo-preview_pbxebs.png" 
    style="
    border-spacing: 0;
    border-collapse: collapse;
    color: #222;
    direction: ltr;
    font: small/1.5 Arial,Helvetica,sans-serif;
    font-family: 'Roboto',sans-serif;
    font-size: 14px;
    border-top-width: 0px;
    border-bottom-width: 0px;
    border-left-width: 0px;
    border-top-style: solid;
    border-bottom-style: solid;
    border-left-style: solid;
    height: 86px;
    padding-right: 2px;
    border-right: 1px black solid;
    "/>
    </div>
    <div style="
       border-spacing: 0;
       border-collapse: collapse;
       color: #222;
       direction: ltr;
       font: small/1.5 Arial,Helvetica,sans-serif;
       font-family: 'Roboto',sans-serif;
       font-size: 14px;
       padding-left: 20px;
    ">
    <p
       style="
          border-spacing: 0;
          border-collapse: collapse;
          direction: ltr;
          font: small/1.5 Arial,Helvetica,sans-serif;
          font-family: 'Roboto',sans-serif;
          font-size: 14px;
          font-weight:bold;
          margin: 0;
          padding: 0;
          margin-bottom: 4px;
       "
    >${receipient}</p>
    <p
    style="
       border-spacing: 0;
       border-collapse: collapse;
       direction: ltr;
       font: small/1.5 Arial,Helvetica,sans-serif;
       font-family: 'Roboto',sans-serif;
       font-size: 14px;
       margin: 0;
       padding: 0;
       margin-bottom: 4px;
    "
 > Asetech Academy</p>
    <a 
          href="mailto:info@asetech.academy
          style="
          border-spacing: 0;
          border-collapse: collapse;
          direction: ltr;
          font: small/1.5 Arial,Helvetica,sans-serif;
          font-family: 'Roboto',sans-serif;
          color: #0099ff;
          margin-bottom: 2px;
          text-decoration: underline;
          font-size: 14px;
          margin-top: 14px;
    ">info@asetech.academy</a>
    </div>
    </div>
    `,
  });

  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info, "info"));

  return await info.messageId;
};

exports.mailerFunc = mailerFunc;
