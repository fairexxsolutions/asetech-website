

exports.user_message = (name, programme ,course) => {

    const message = `
        <h1>Confirmation Message for Registeration of ${programme}</h1>
        <br/>
        <p style="font-size:14px">Hi ${name},</p>
        <p style="font-size:14px">
            You have successfully registered for <em><strong>${course}</strong></em> 
            course at <em><strong>${programme}</strong></em>. 
            We are glad you registered with us. We will soon reach out
            to you for the next steps to take in relations to this course. 
        </p>
`;
    return message;
}


exports.admin_message = (name,programme, sheetTile) => {

    const message = `
        <h1>${programme}</h1>
        <br/>
        <p style="font-size:14px">Hi there,</p>
        <p style="font-size:14px">
        <strong>${name}</strong> has registered for the ${programme}.
        Reach out to them till the deal is sealed. You can check out more information about ${name} at
        the google sheet titled - <b>"${sheetTile}"</b>.
        </p>          
`;

    return message;
}