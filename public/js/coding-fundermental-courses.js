let cdc = document.querySelector("#cfc-submit");
cdc.addEventListener("click", cfcProcessor);

function cfcProcessor(e) {
  e.preventDefault();
  let name = document.querySelector("#cdc-name");
  let email = document.querySelector("#cdc-email");
  let number = document.querySelector("#cdc-number");
  let birthday = document.querySelector("#cdc-birthday");
  let courses = document.querySelector("#cdc-courses");
  let date = new Date();

  let user = {
    name: name.value,
    email: email.value,
    number: number.value,
    birthday: birthday.value,
    course: courses.value,
    createdAt: `Created at: ${date.getDate()}:${date.getMonth()}:${date.getFullYear()}`
  };
  

  const validate = () => {
    if (user.name === "") {
      inValid("name", `Please fill in your name`);
    } else inValid("name", ``);
  

  if (user.email === "") {
    inValid("email", `Please fill in your first name`);
  } else inValid("email", ``);

  
  if (user.number === "") {
    inValid("number", `Please fill your phone number`);
  } else inValid("number", ``)

  if (user.course === "Choose") {
    inValid("select-course", `Please choose your course`);
  } else inValid("select-course", ``);


  if (
    user.name !== "" &&
    user.number !== "" &&
    user.email !== "" &&
    emailExactPattern(email) &&
    user.course !== "Choose"
  )
    return true;
  else return false;

  }



  if(validate()) {
    console.log("sending")

    axios
    .post("/coding-fundamental-programme/submit", user)
    .then((response) => {
      const addedUser = response.data;
      console.log(addedUser);
      localStorage.setItem(
        "paymentLinks",
        JSON.stringify({
          status: addedUser.status,
        })
      );
      window.location.pathname = addedUser.path;
    })
    .catch((error) => console.error(error));
  }



  function emailExactPattern(element) {
    const regex =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regex.test(element.value)) {
      return true;
    }
  }

  function inValid(element, message) {
    let input = document.getElementsByName(element)[0];
    input.nextElementSibling.innerHTML = message;
    input.nextElementSibling.style.color = "rgb(238, 50, 50)";
  }



}
