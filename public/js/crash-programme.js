let c = document.querySelector("#c-submit");
c.addEventListener("click", cProcessor);

function cProcessor(e) {
  e.preventDefault();
  let name = document.querySelector("#c-name");
  let email = document.querySelector("#c-email");
  let number = document.querySelector("#c-number");
  let dateofbith = document.querySelector("#c-dateofbirth");
  let course = document.querySelector("#c-courses");

  let user = {
    name: name.value,
    email: email.value,
    number: number.value,
    dateofbith: dateofbith.value,
    course: course.value,
    createdAt: `Created at: day: ${date.getDate()}, Month: ${date.getMonth()}, Year: ${date.getFullYear()}`
  };



  const validate = () => {
    if (user.name === "") {
      inValid("name", `Please fill your name`);
    } else inValid("name", ``);

    if (user.email === "" || !emailExactPattern(email)) {
      inValid("email", `Please fill or provide a valid email`);
    } else inValid("email", ``);

    if (user.number === "") {
      inValid("number", `Please fill your phone number`);
    } else inValid("number", ``);

    if (user.course === "Choose") {
      inValid("course-container", `Please choose your course`);
    } else inValid("course-container", ``);



    if (
      user.name !== "" &&
      user.email !== "" &&
      emailExactPattern(email) &&
      user.number !== "" &&
      user.course !== "Choose"
    )
      return true;
    else return false;
  }


  if(validate()) {
    console.log(user, "sending");
    axios
    .post("/general-course/submit", user)
    .then((response) => {
      const addedUser = response.data;
      localStorage.setItem(
        "paymentLinks",
        JSON.stringify({
          status: addedUser.status,
          course: course.value,
        })
      );
      window.location.pathname = addedUser.path;
    })
    .catch((error) => console.error(error));
  }

  
  function emailExactPattern(element) {
    const regex =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regex.test(element.value)) {
      return true;
    }
  }

  function inValid(element, message) {
    let input = document.getElementsByName(element)[0];
    input.nextElementSibling.innerHTML = message;
    input.nextElementSibling.style.color = "rgb(238, 50, 50)";
  }
}
