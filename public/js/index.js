let suscribeButton = document.querySelector("#suscribe-button");
let successMessageDiv = document.getElementById("suscription-message");
let allSubsInput = document.querySelectorAll(".subs-input");
let form = document.querySelector("#suscribe-form");
let sub = "sub-name";

form.addEventListener("submit", function (e) {
  e.preventDefault();

  const [firstName, lastName, email] = e.target.elements;

  let user = {
    firstName: firstName.value,
    lastName: lastName.value,
    email: email.value,
    createdAt: `Created at: day: ${date.getDate()}, Month: ${date.getMonth()}, Year: ${date.getFullYear()}`
  };

  const validate = () => {
    if (firstName.value === "") {
      inValid("firstName", `Please fill your firstName`);
    } else inValid("firstName", ``);

    if (lastName.value === "") {
      inValid("lastName", `Please fill your lastName`);
    } else inValid("lastName", ``);

    if (email.value === "" || !emailExactPattern(email)) {
      inValid("email", `Please fill or provide a valid email`);
    } else inValid("email", ``);

    if (
      firstName.value !== "" &&
      lastName.value !== "" &&
      email.value !== "" &&
      emailExactPattern(email)
    ) {
      return true;
    } else return false;
  };

  if (validate()) {
    axios
      .post("/suscribe", user)
      .then((response) => {
        allSubsInput[0].value = "";
        allSubsInput[1].value = "";
        allSubsInput[2].value = "";

        const addedUser = response.data;
        successMessageDiv.style.display = "block";
        successMessageDiv.style.background = "rgba(1,166,130,1)";
        successMessageDiv.innerHTML = `${addedUser.message} <span id="subs-close" style="display: block; position: absolute; top:20px; right:40px; cursor: pointer;">x</span>`;

        let subsClose = document.getElementById("subs-close");
        subsClose.addEventListener("click", function (e) {
          successMessageDiv.style.display = "none";
        });
      })
      .catch((error) => {
        let err = JSON.parse(error.response.data.message);
        successMessageDiv.style.display = "block";
        successMessageDiv.style.background = "rgb(9, 59, 88)";
        if (err.title === "Member Exists") {
          successMessageDiv.innerHTML = `You have already subscribed with this email <span id="subs-close" style="display: block; position: absolute; top:20px; right:40px; cursor: pointer;">x</span>`;
        } else {
          successMessageDiv.innerHTML = `${err.status}  ${err.title} <span id="subs-close" style="display: block; position: absolute; top:20px; right:40px; cursor: pointer;">x</span>`;
        }

        let subsClose = document.getElementById("subs-close");

        subsClose.addEventListener("click", function (e) {
          successMessageDiv.style.display = "none";
        });
      });
  }
});

function emailExactPattern(element) {
  const regex =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (regex.test(element.value)) {
    return true;
  }
}

function inValid(element, message) {
  let input = document.getElementsByName(element)[0];
  input.nextElementSibling.innerHTML = message;
  input.nextElementSibling.style.color = "rgb(238, 50, 50)";
}
