function loadSuccessPage(elem) {
  let local = localStorage.getItem("paymentLinks");

  if (!local) {
    window.location.replace(elem.location.origin);
  } else {
    let localData = JSON.parse(local.toString());
    document.getElementsByClassName("body")[0].style.display = "block";

    let cRow = document.getElementById("c-row");

    switch (localData.course) {
      case "Python":
        cRow.innerHTML = `
                    <div class="c-card m-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); border-radius: 4px; border:1px solid #e5e5e5; padding: 10px; width: fit-content;">
                    <div class="c-card" style=" height: 100%; width: 290px !important; padding:60px 20px; border-radius: 4px; border:1px solid #e5e5e5; padding-top: 30px; padding-bottom: 30px; width: fit-content;">
                    <p style="color: rgb(40, 119, 40); font-size: 20px; margin-bottom:10px;">Python Payment Plan</p> 
                    <h1 style="font-weight: 600; font-size:50px;">&#8358; 135,000</h1>   
                    <div class="blog-btn">
                        <ul>
                            <li style="font-size:14px; list-style: inherit; margin-left: 15px;">For a 2 month course</li>
                        </ul>
                        <img src="./img/icon/flutterwave.png" width="150"style="margin-top:10px; margin-bottom: 114px;" alt="flutterwave_logo"/> <br>
                        <a class="mm-btn" style="margin-top: 0 !important; margin-bottom: 0 !important; padding: 10px 15px; border-radius: 5px; color: #ffffff; text-decoration: none; width:100%;" href="https://flutterwave.com/pay/mf1drmpriwnv">Proceed with the payment</a>
                </div>
                </div>
            </div>
            `;
        return;

      case "Java":
        cRow.innerHTML = `
        <div class="c-card m-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); border-radius: 4px; border:1px solid #e5e5e5; padding: 10px; width: fit-content;">
        <div class="c-card" style=" height: 100%; width: 290px !important; padding:60px 20px; border-radius: 4px; border:1px solid #e5e5e5; padding-top: 30px; padding-bottom: 30px; width: fit-content;">
        <p style="color: rgb(40, 119, 40); font-size: 20px; margin-bottom:10px;">Java Payment Plan</p> 
        <h1 style="font-weight: 600; font-size:50px;">&#8358; 165,000</h1>   
        <div class="blog-btn">
            <ul>
                <li style="font-size:14px; list-style: inherit; margin-left: 15px;">For a 2 month course</li>
            </ul>
            <img src="./img/icon/flutterwave.png" width="150"style="margin-top:10px; margin-bottom: 114px;" alt="flutterwave_logo"/> <br>
            <a class="mm-btn" style="margin-top: 0 !important; margin-bottom: 0 !important; padding: 10px 15px; border-radius: 5px; color: #ffffff; text-decoration: none; width:100%;" href="https://flutterwave.com/pay/bwlnlyqcbglk">Proceed with the payment</a>
    </div>
    </div>
</div>
`;

        return;

      case "JavaScript":
        cRow.innerHTML = `
    <div class="c-card m-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); border-radius: 4px; border:1px solid #e5e5e5; padding: 10px; width: fit-content;">
    <div class="c-card" style=" height: 100%; width: 290px !important; padding:60px 20px; border-radius: 4px; border:1px solid #e5e5e5; padding-top: 30px; padding-bottom: 30px; width: fit-content;">
    <p style="color: rgb(40, 119, 40); font-size: 20px; margin-bottom:10px;">JavaScript Payment Plan</p> 
    <h1 style="font-weight: 600; font-size:50px;">&#8358; 80,000</h1>   
    <div class="blog-btn">
        <ul>
            <li style="font-size:14px; list-style: inherit; margin-left: 15px;">For a 2 month course</li>
        </ul>
        <img src="./img/icon/flutterwave.png" width="150"style="margin-top:10px; margin-bottom: 114px;" alt="flutterwave_logo"/> <br>
        <a class="mm-btn" style="margin-top: 0 !important; margin-bottom: 0 !important; padding: 10px 15px; border-radius: 5px; color: #ffffff; text-decoration: none; width:100%;" href="https://flutterwave.com/pay/nbic3e1xj2ha">Proceed with the payment</a>
</div>
</div>
</div>
`;

        return;

      case "Node.js":
        cRow.innerHTML = `
    <div class="c-card m-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); border-radius: 4px; border:1px solid #e5e5e5; padding: 10px; width: fit-content;">
    <div class="c-card" style=" height: 100%; width: 290px !important; padding:60px 20px; border-radius: 4px; border:1px solid #e5e5e5; padding-top: 30px; padding-bottom: 30px; width: fit-content;">
    <p style="color: rgb(40, 119, 40); font-size: 20px; margin-bottom:10px;">Node.js Payment Plan</p> 
    <h1 style="font-weight: 600; font-size:50px;">&#8358; 40,000</h1>   
    <div class="blog-btn">
        <ul>
            <li style="font-size:14px; list-style: inherit; margin-left: 15px;">For a 2 month course</li>
        </ul>
        <img src="./img/icon/flutterwave.png" width="150"style="margin-top:10px; margin-bottom: 114px;" alt="flutterwave_logo"/> <br>
        <a class="mm-btn" style="margin-top: 0 !important; margin-bottom: 0 !important; padding: 10px 15px; border-radius: 5px; color: #ffffff; text-decoration: none; width:100%;" href="https://flutterwave.com/pay/jbphiwei2gt6">Proceed with the payment</a>
</div>
</div>
</div>
`;

        return;

      case "PHP":
        cRow.innerHTML = `
    <div class="c-card m-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); border-radius: 4px; border:1px solid #e5e5e5; padding: 10px; width: fit-content;">
    <div class="c-card" style=" height: 100%; width: 290px !important; padding:60px 20px; border-radius: 4px; border:1px solid #e5e5e5; padding-top: 30px; padding-bottom: 30px; width: fit-content;">
    <p style="color: rgb(40, 119, 40); font-size: 20px; margin-bottom:10px;">PHP Payment Plan</p> 
    <h1 style="font-weight: 600; font-size:50px;">&#8358; 100,000</h1>   
    <div class="blog-btn">
        <ul>
            <li style="font-size:14px; list-style: inherit; margin-left: 15px;">For a 2 month course</li>
        </ul>
        <img src="./img/icon/flutterwave.png" width="150"style="margin-top:10px; margin-bottom: 114px;" alt="flutterwave_logo"/> <br>
        <a class="mm-btn" style="margin-top: 0 !important; margin-bottom: 0 !important; padding: 10px 15px; border-radius: 5px; color: #ffffff; text-decoration: none; width:100%;" href="https://flutterwave.com/pay/ncykloeasacy">Proceed with the payment</a>
</div>
</div>
</div>
`;

        return;

      case "HTML/CSS":
        cRow.innerHTML = `
    <div class="c-card m-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); border-radius: 4px; border:1px solid #e5e5e5; padding: 10px; width: fit-content;">
    <div class="c-card" style=" height: 100%; width: 290px !important; padding:60px 20px; border-radius: 4px; border:1px solid #e5e5e5; padding-top: 30px; padding-bottom: 30px; width: fit-content;">
    <p style="color: rgb(40, 119, 40); font-size: 20px; margin-bottom:10px;">HTML/CSS Payment Plan</p> 
    <h1 style="font-weight: 600; font-size:50px;">&#8358; 40,000</h1>   
    <div class="blog-btn">
        <ul>
            <li style="font-size:14px; list-style: inherit; margin-left: 15px;">For a 2 month course</li>
        </ul>
        <img src="./img/icon/flutterwave.png" width="150"style="margin-top:10px; margin-bottom: 114px;" alt="flutterwave_logo"/> <br>
        <a class="mm-btn" style="margin-top: 0 !important; margin-bottom: 0 !important; padding: 10px 15px; border-radius: 5px; color: #ffffff; text-decoration: none; width:100%;" href="https://flutterwave.com/pay/baqnq0cdpgbb">Proceed with the payment</a>
</div>
</div>
</div>
`;

        return;

      case "React.js":
        cRow.innerHTML = `
    <div class="c-card m-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); border-radius: 4px; border:1px solid #e5e5e5; padding: 10px; width: fit-content;">
    <div class="c-card" style=" height: 100%; width: 290px !important; padding:60px 20px; border-radius: 4px; border:1px solid #e5e5e5; padding-top: 30px; padding-bottom: 30px; width: fit-content;">
    <p style="color: rgb(40, 119, 40); font-size: 20px; margin-bottom:10px;">React.js Payment Plan</p> 
    <h1 style="font-weight: 600; font-size:50px;">&#8358; 40,000</h1>   
    <div class="blog-btn">
        <ul>
            <li style="font-size:14px; list-style: inherit; margin-left: 15px;">For a 2 month course</li>
        </ul>
        <img src="./img/icon/flutterwave.png" width="150"style="margin-top:10px; margin-bottom: 114px;" alt="flutterwave_logo"/> <br>
        <a class="mm-btn" style="margin-top: 0 !important; margin-bottom: 0 !important; padding: 10px 15px; border-radius: 5px; color: #ffffff; text-decoration: none; width:100%;" href="https://flutterwave.com/pay/jetb5ts2faln">Proceed with the payment</a>
</div>
</div>
</div>
`;

        return;

      case "Database Design":
        cRow.innerHTML = `
    <div class="c-card m-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); border-radius: 4px; border:1px solid #e5e5e5; padding: 10px; width: fit-content;">
    <div class="c-card" style=" height: 100%; width: 290px !important; padding:60px 20px; border-radius: 4px; border:1px solid #e5e5e5; padding-top: 30px; padding-bottom: 30px; width: fit-content;">
    <p style="color: rgb(40, 119, 40); font-size: 20px; margin-bottom:10px;">Database Design Payment Plan</p> 
    <h1 style="font-weight: 600; font-size:50px;">&#8358; 40,000</h1>   
    <div class="blog-btn">
        <ul>
            <li style="font-size:14px; list-style: inherit; margin-left: 15px;">For a 2 month course</li>
        </ul>
        <img src="./img/icon/flutterwave.png" width="150"style="margin-top:10px; margin-bottom: 114px;" alt="flutterwave_logo"/> <br>
        <a class="mm-btn" style="margin-top: 0 !important; margin-bottom: 0 !important; padding: 10px 15px; border-radius: 5px; color: #ffffff; text-decoration: none; width:100%;" href="https://flutterwave.com/pay/e6vlbto23i3u">Proceed with the payment</a>
</div>
</div>
</div>
`;

        return;

      case "Microsoft Office":
        cRow.innerHTML = `
    <div class="c-card m-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); border-radius: 4px; border:1px solid #e5e5e5; padding: 10px; width: fit-content;">
    <div class="c-card" style=" height: 100%; width: 290px !important; padding:60px 20px; border-radius: 4px; border:1px solid #e5e5e5; padding-top: 30px; padding-bottom: 30px; width: fit-content;">
    <p style="color: rgb(40, 119, 40); font-size: 20px; margin-bottom:10px;">Microsoft Office Payment Plan</p> 
    <h1 style="font-weight: 600; font-size:50px;">&#8358; 45,000</h1>   
    <div class="blog-btn">
        <ul>
            <li style="font-size:14px; list-style: inherit; margin-left: 15px;">For a 2 month course</li>
        </ul>
        <img src="./img/icon/flutterwave.png" width="150"style="margin-top:10px; margin-bottom: 114px;" alt="flutterwave_logo"/> <br>
        <a class="mm-btn" style="margin-top: 0 !important; margin-bottom: 0 !important; padding: 10px 15px; border-radius: 5px; color: #ffffff; text-decoration: none; width:100%;" href="https://flutterwave.com/pay/covluxn7khsv">Proceed with the payment</a>
</div>
</div>
</div>
`;

        return;

      case "Corel Draw":
        cRow.innerHTML = `
    <div class="c-card m-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); border-radius: 4px; border:1px solid #e5e5e5; padding: 10px; width: fit-content;">
    <div class="c-card" style=" height: 100%; width: 290px !important; padding:60px 20px; border-radius: 4px; border:1px solid #e5e5e5; padding-top: 30px; padding-bottom: 30px; width: fit-content;">
    <p style="color: rgb(40, 119, 40); font-size: 20px; margin-bottom:10px;">Corel Draw Payment Plan</p> 
    <h1 style="font-weight: 600; font-size:50px;">&#8358; 45,000</h1>   
    <div class="blog-btn">
        <ul>
            <li style="font-size:14px; list-style: inherit; margin-left: 15px;">For a 2 month course</li>
        </ul>
        <img src="./img/icon/flutterwave.png" width="150"style="margin-top:10px; margin-bottom: 114px;" alt="flutterwave_logo"/> <br>
        <a class="mm-btn" style="margin-top: 0 !important; margin-bottom: 0 !important; padding: 10px 15px; border-radius: 5px; color: #ffffff; text-decoration: none; width:100%;" href="https://flutterwave.com/pay/zp0gxhkjmqn8">Proceed with the payment</a>
</div>
</div>
</div>
`;

        return;

      case "Desktop Publishing":
        cRow.innerHTML = `
    <div class="c-card m-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); border-radius: 4px; border:1px solid #e5e5e5; padding: 10px; width: fit-content;">
    <div class="c-card" style=" height: 100%; width: 290px !important; padding:60px 20px; border-radius: 4px; border:1px solid #e5e5e5; padding-top: 30px; padding-bottom: 30px; width: fit-content;">
    <p style="color: rgb(40, 119, 40); font-size: 20px; margin-bottom:10px;">Desktop Publishing Payment Plan</p> 
    <h1 style="font-weight: 600; font-size:50px;">&#8358; 45,000</h1>   
    <div class="blog-btn">
        <ul>
            <li style="font-size:14px; list-style: inherit; margin-left: 15px;">For a 2 month course</li>
        </ul>
        <img src="./img/icon/flutterwave.png" width="150"style="margin-top:10px; margin-bottom: 114px;" alt="flutterwave_logo"/> <br>
        <a class="mm-btn" style="margin-top: 0 !important; margin-bottom: 0 !important; padding: 10px 15px; border-radius: 5px; color: #ffffff; text-decoration: none; width:100%;" href="https://flutterwave.com/pay/zp0gxhkjmqn8">Proceed with the payment</a>
</div>
</div>
</div>
`;

        return;

      default:
        break;
    }
  }
}
