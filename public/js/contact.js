let cdpForm = document.querySelector("#contact-form");
cdpForm.addEventListener("submit", contactProcessor);

function contactProcessor(e) {
  e.preventDefault();

  const [firstName, lastName, email, phone, message] = e.target.elements;

  let user = {
    firstName: firstName.value,
    lastName: lastName.value,
    email: email.value,
    phone: phone.value,
    message: message.value,
  };

  const validate = () => {
    if (firstName.value === "") {
      inValid("firstName", `Please fill your firstName`);
    } else inValid("firstName", ``);

    if (lastName.value === "") {
      inValid("lastName", `Please fill your lastName`);
    } else inValid("lastName", ``);

    if (email.value === "" || !emailExactPattern(email)) {
      inValid("email", `Please fill or provide a valid email`);
    } else inValid("email", ``);

    if (phone.value === "") {
      inValid("phone", `Please fill your phone`);
    } else inValid("phone", ``);

    if (message.value === "") {
      inValid("message", `Please fill your message`);
    } else inValid("message", ``);

    if (
      firstName.value !== "" &&
      lastName.value !== "" &&
      email.value !== "" &&
      emailExactPattern(email) &&
      phone.value !== "" &&
      message.value !== ""
    ) {
      return true;
    } else return false;
  };

  if (validate()) {
    axios
      .post("/contact-submit", user)
      .then((response) => {
        let success = document.getElementById("suscription-message");
        const messageResponse = response.data;
        success.style.display = "block";

        if (messageResponse.responseCode !== 200) {
          success.innerHTML = `<div class="message-div" id="suscription-message" style="display:none; background-color: red; text-align: center; width: 100%; padding: 20px 20px; z-index:400; font-size:18px; font-weight:400; position: fixed; bottom: 0; color: #ffffff;">Message Not Sent <span id="subs-close" style="display: block; position: absolute; top:20px; right:40px; cursor: pointer;">x</span></div>`;
          return;
        }

        success.innerHTML = `<div class="message-div" id="suscription-message" style="display:none; background-color: rgba(1,166,130,1); text-align: center; width: 100%; padding: 20px 20px; z-index:400; font-size:18px; font-weight:400; position: fixed; bottom: 0; color: #ffffff;">Message Sent <span id="subs-close" style="display: block; position: absolute; top:20px; right:40px; cursor: pointer;">x</span></div>`;

        let subsClose = document.getElementById("subs-close");
        subsClose.addEventListener("click", function (e) {
          success.style.display = "none";
        });

        setTimeout(() => {
          window.location.reload();
        }, 5000);
      })
      .catch((error) => console.error(error, "error"));
  }

  function emailExactPattern(element) {
    const regex =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regex.test(element.value)) {
      return true;
    }
  }

  function inValid(element, message) {
    let input = document.getElementsByName(element)[0];
    input.nextElementSibling.innerHTML = message;
    input.nextElementSibling.style.color = "rgb(238, 50, 50)";
  }
}
