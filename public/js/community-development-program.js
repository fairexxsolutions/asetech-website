let cdp = document.querySelector("#cdp-submit");
cdp.addEventListener("click", cdpProcessor);
let cdpRadio;

function getRadioValues(tag) {
  cdpRadio = tag.value;
}

function cdpProcessor(e) {
  e.preventDefault();
  let parentName = document.querySelector("#cdp-parentName");
  let email = document.querySelector("#cdp-email");
  let number = document.querySelector("#cdp-number");
  let address = document.querySelector("#cdp-address");
  let childName = document.querySelector("#cdp-childName");
  let childSchool = document.querySelector("#cdp-childSchool");
  let birthDate = document.querySelector("#cdp-birthdate");
  let courses = document.querySelector("#cdp-courses");
  let childInfo = document.querySelector("#cdp-childInfo");
  let date = new Date();

  let user = {
    parentName: parentName.value,
    email: email.value,
    number: number.value,
    address: address.value,
    childName: childName.value,
    childSchool: childSchool.value,
    radio: cdpRadio,
    birthDate: birthDate.value,
    courses: courses.value,
    childInfo: childInfo.value,
    createdAt: `Created at: ${date.getDate()}:${date.getMonth()}:${date.getFullYear()}`
  };

  const validate = () => {
    if (user.parentName === "") {
      inValid("parentName", `Please fill your name`);
    } else inValid("parentName", ``);

    if (user.email === "" || !emailExactPattern(email)) {
      inValid("email", `Please fill or provide a valid email`);
    } else inValid("email", ``);

    if (user.number === "") {
      inValid("phoneNumber", `Please fill your phone number`);
    } else inValid("phoneNumber", ``);

    if (user.address === "") {
      inValid("contactAddress", `Please fill your address`);
    } else inValid("contactAddress", ``);

    if (user.childName === "") {
      inValid("childName", `Please fill your child name`);
    } else inValid("childName", ``);

    if (!user.radio) {
      inValid("seniorSecondary", `Please choose your child's class`);
    } else inValid("seniorSecondary", ``);

    if (user.courses === "Choose") {
      inValid("select-container", `Please choose your child's course`);
    } else inValid("select-container", ``);

    if (
      user.parentName !== "" &&
      user.email !== "" &&
      emailExactPattern(email) &&
      user.number !== "" &&
      user.address !== "" &&
      user.childName !== "" &&
      user.radio &&
      user.courses !== "Choose"
    )
      return true;
    else return false;
  };

  if (validate()) {
    axios
      .post("/community-development-programme/submit", user)
      .then((response) => {
        const addedUser = response.data;

        
        localStorage.setItem(
          "paymentLinks",
          JSON.stringify({
            status: addedUser.status,
            courses: courses.value,
          })
        );
        window.location.pathname = addedUser.path;
      })
      .catch((error) => console.error(error));
  }

  function emailExactPattern(element) {
    const regex =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regex.test(element.value)) {
      return true;
    }
  }

  function inValid(element, message) {
    let input = document.getElementsByName(element)[0];
    input.nextElementSibling.innerHTML = message;
    input.nextElementSibling.style.color = "rgb(238, 50, 50)";
  }
}
