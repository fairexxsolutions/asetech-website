let school = document.querySelector(".school");
let schoolOutput = document.querySelector(".schoolOutput");
let monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];


window.addEventListener("click", (e) => {
   if(e.target.className !== "school" || e.target.className !== "school-name") {
      schoolOutput.innerHTML = ``; 
      schoolOutput.style.display = "none";
   }
   else return;
})

// Filtering the schools

  axios.get("/msoschools").then(data => {
    
    school.addEventListener('keyup', function(e) {
        e.preventDefault();

        data.data.data.schools.forEach((school) => {
          if(e.target.value === "") {
            schoolOutput.style.display = "none";
            return schoolOutput.innerHTML = ``; 
          }
        

          if(e.target.value !== "" && school.name.toLowerCase().includes(e.target.value.toLowerCase())){
            schoolOutput.style.display = "block";
            return schoolOutput.innerHTML = `<div class="school-name">${school.name}</div>`;
          }

        })

    })

  });

     
  
  
// Getting the school name using onclick function

const setValueToInput = (e) => {
  school.value = e.children[0].innerText;
  schoolOutput.innerHTML = ``; 
  schoolOutput.style.display = "none";
}



  let kodeClubForm = document.querySelector("#kodeclub-form")
  .addEventListener("submit", formProcessor);


// Submitting the form data

function formProcessor(e) {
  e.preventDefault();

  const [parentName, email, number, child, school, classOfChild, timeSlot, dateSlot] = e.target.elements;
  let date = new Date();


  let user = {
    parentName: parentName.value,
    email: email.value,
    number: number.value,
    child: child.value,
    school: school.value,
    classOfChild: classOfChild.value,
    timeSlot: timeSlot.value,
    dateSlot: dateSlot.value,
    createdAt: `Created at: ${date.getDay()}:${date.getMonth() + 1}:${date.getFullYear()}`
  };


  console.log(parseInt(user.dateSlot.split(" ")[0].replace(/[A-Za-z]/gm, ""), 10), "", date.getDate(), "dateslot");

  let regex = /^(?:(?:(?:\+?234(?:\h1)?|01)\h*)?(?:\(\d{3}\)|\d{3})|\d{4})(?:\W*\d{3})?\W*\d{4}$/;


  const validate = () => {
    if (parentName.value === "") {
      inValid("parentName", `Please fill your name`);
    } else inValid("parentName", ``);

    if (email.value === "" || !emailExactPattern(email)) {
      inValid("email", `Please fill or provide a valid email`);
    } else inValid("email", ``);

    if (regex.test(number.value) === false) {
      inValid("number", `Invalid Phone Number`);
    } else inValid("number", ``);

    if (child.value === "") {
      inValid("child", `Please fill your child's name`);
    } else inValid("child", ``);

    if (school.value === "") {
      inValid("school", `Please fill your child's school`);
    } else inValid("school", ``);

    if (classOfChild.value === "Choose") {
      inValid("classOfChild", `Please fill your child's class`);
    } else inValid("classOfChild", ``);

    if (timeSlot.value === "Choose") {
      inValid("timeSlot", `Please select the time schedule.`);
    } else inValid("timeSlot", ``);

    if (dateSlot.value === "Choose" || parseInt(user.dateSlot.split(" ")[0].replace(/[A-Za-z]/gm, ""), 10) < date.getDate()) {
      inValid("dateSlot", `Please select the current date schedule.`);
    } else inValid("dateSlot", ``);


    if (
      parentName.value !== "" &&
      email.value !== "" &&
      emailExactPattern(email) &&
      number.value.length === 11 &&
      child.value !== "" &&
      school.value !== "" &&
      classOfChild.value !== "Choose" &&
      timeSlot.value !== "Choose" &&
      dateSlot.value !== "Choose"
    ) {
      return true;
    } else return false;
  };

  if (validate()) {
    console.log("Yes. Sending......")
    axios
      .post("/kodeklub/submit", user)
      .then((response) => {
        let responseTag = document.getElementById("kodeclub-message-container");
        let responseChildTag = document.querySelector(".kodeclub-message");

        if (response.data.status === "error") {
          const res = response.data;
          localStorage.setItem(
            "unsuccessful",
            JSON.stringify({
              status: res.status,
            })
          );
          window.location.pathname = res.path;
          return;
        }

        if (response.data.status === "matched") {
          responseChildTag.innerHTML = `
          <i class="fa fa-exclamation-circle"></i>
           ${response.data.message}
          `;
          responseTag.classList.add("modal-show");
          return;
        }


        responseChildTag.innerHTML = `
        <i class="fas fa-check-circle"></i>
        ${response.data.message}
 `;
        responseTag.classList.add("modal-show");

      })
      .catch((error) => {
        let responseTag = document.getElementById("kodeclub-message-container");
        let responseChildTag = document.querySelector(".kodeclub-message");

        responseChildTag.innerHTML = `
        <i class="fa fa-times-circle error-icon"></i>
        Your message was not Sent
        `;
        responseTag.classList.add("modal-show");
      });
  }

  let close = document.querySelector(".fa-times-circle");
  let modalClose = document.querySelector("#kodeclub-message-container")

  close.addEventListener("click", () => document.getElementById("kodeclub-message-container").classList.remove("modal-show"));
  modalClose.addEventListener("click", (e) => e.target.id === 'kodeclub-message-container' ? document.getElementById("kodeclub-message-container").classList.remove("modal-show") : false);


  function emailExactPattern(element) {
    const regex =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regex.test(element.value)) {
      return true;
    }
  }

  function inValid(element, message) {
    let input = document.getElementsByName(element)[0];
    input.nextElementSibling.innerHTML = message;
    input.nextElementSibling.style.color = "rgb(238, 50, 50)";
  }
}


//Section of Dates

function daysInMonth(month,year) {
  return new Date(year, month, 0).getDate();
}

let date = new Date();
let totalDaysInAMonth = daysInMonth(date.getMonth(),date.getFullYear());
let Saturday = new Array();



for(var i=1;i<=totalDaysInAMonth;i++){ 
  var newDate = new Date(date.getFullYear(),date.getMonth(),i);

  if(newDate.getDay() == 6 ){ 
      Saturday.push(i);
  }

}


let dateSlot = document.querySelector(".dateSlot");

Saturday.map(day=> {
  let option = document.createElement("option");
  option.value = day;
  option.value = day === 1 ? `${day}st, Saturday, ${monthNames[date.getMonth()]} ${date.getFullYear()}` : day === 2 ? `${day}nd, Saturday, ${monthNames[date.getMonth()]} ${date.getFullYear()}` : day === 3 ? `${day}rd, Saturday, ${monthNames[date.getMonth()]} ${date.getFullYear()}` : `${day}th, Saturday, ${monthNames[date.getMonth()]} ${date.getFullYear()}`;
  option.innerHTML = day === 1 ? `${day}st, Saturday, ${monthNames[date.getMonth()]} ${date.getFullYear()}` : day === 2 ? `${day}nd, Saturday, ${monthNames[date.getMonth()]} ${date.getFullYear()}` : day === 3 ? `${day}rd, Saturday, ${monthNames[date.getMonth()]} ${date.getFullYear()}` : `${day}th, Saturday, ${monthNames[date.getMonth()]} ${date.getFullYear()}`;
  dateSlot.appendChild(option); 
})
