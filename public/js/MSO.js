function loadUnSuccessMSOPage(elem) {
    let local = localStorage.getItem("unsuccessful");
    if (!local) {
      window.location.replace(elem.location.origin);
    } else {
      let localData = JSON.parse(local.toString());
      document.getElementsByClassName("body")[0].style.display = "block";
    }

}



document.querySelector("#mso-form")
.addEventListener("submit", formProcessor);


// Submitting the form data

function formProcessor(e) {
e.preventDefault();

const [ schoolName, address, contactName, number, email ] = e.target.elements;
let date = new Date();


let user = {
  school: schoolName.value,
  address: address.value,
  contactName: contactName.value,
  number: number.value,
  email: email.value,
  createdAt: `Created at: ${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
};

let regex = /^(?:(?:(?:\+?234(?:\h1)?|01)\h*)?(?:\(\d{3}\)|\d{3})|\d{4})(?:\W*\d{3})?\W*\d{4}$/;


const validate = () => {

  if (user.school === "") {
    inValid("schoolName", `Please fill in school`);
  } else inValid("schoolName", ``);

  if (number.value !== "" && regex.test(number.value) === false) {
    inValid("number", `Invalid Phone Number`);
  } else inValid("number", ``);

  if (email.value === "" || !emailExactPattern(email)) {
    inValid("email", `Please fill or provide a valid email`);
  } else inValid("email", ``);

 
  if (
    user.school !== ""
    && emailExactPattern(email) 
  ) {
    return true;
  } else return false;
};

if (validate()) {
  axios
    .post("/kodeklub/unregisteredschools", user)
    .then((response) => {
      let responseTag = document.getElementById("kodeclub-message-container");
      let responseChildTag = document.querySelector(".kodeclub-message");

      if (response.data.status === "error") {
        const res = response.data;
        localStorage.setItem(
          "unsuccessful",
          JSON.stringify({
            status: res.status,
          })
        );
        window.location.pathname = res.path;
      }

      if (response.data.status === "matched") {
        responseChildTag.innerHTML = `
        <i class="fa fa-exclamation-circle"></i>
         ${response.data.message}
        `;
        responseTag.classList.add("modal-show");
        return;
      }




      responseChildTag.innerHTML = `
      <i class="fas fa-check-circle"></i>
      ${response.data.message}
`;
      responseTag.classList.add("modal-show");

    })
    .catch((error) => {
      let responseTag = document.getElementById("kodeclub-message-container");
      let responseChildTag = document.querySelector(".kodeclub-message");

      responseChildTag.innerHTML = `
      <i class="fa fa-times-circle error-icon"></i>
      Your message was not Sent
      `;
      responseTag.classList.add("modal-show");
    });
}

let close = document.querySelector(".fa-times-circle");
let modalClose = document.querySelector("#kodeclub-message-container")

close.addEventListener("click", () => document.getElementById("kodeclub-message-container").classList.remove("modal-show"));
modalClose.addEventListener("click", (e) => e.target.id === 'kodeclub-message-container' ? document.getElementById("kodeclub-message-container").classList.remove("modal-show") : false);


function emailExactPattern(element) {
  const regex =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (regex.test(element.value)) {
    return true;
  }
}

function inValid(element, message) {
  let input = document.getElementsByName(element)[0];
  input.nextElementSibling.innerHTML = message;
  input.nextElementSibling.style.color = "rgb(238, 50, 50)";
}
}
