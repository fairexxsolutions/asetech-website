let cap = document.querySelector("#cap-submit");
cap.addEventListener("click", capProcessor);

function capProcessor(e) {
  e.preventDefault();
  let name = document.querySelector("#cap-name");
  let email = document.querySelector("#cap-email");
  let number = document.querySelector("#cap-number");
  let dateofbith = document.querySelector("#cap-dateofbirth");
  let courses = document.querySelector("#cap-courses");
  let date = new Date();

  let user = {
    name: name.value,
    email: email.value,
    number: number.value,
    dateofbith: dateofbith.value,
    course: courses.value,
    createdAt: `Created at: ${date.getDate()}:${date.getMonth()}:${date.getFullYear()}`
  };


  const validate = () => {
    if (user.name === "") {
      inValid("name", `Please fill your name`);
    } else inValid("name", ``);

    if (user.email === "" || !emailExactPattern(email)) {
      inValid("email", `Please fill or provide a valid email`);
    } else inValid("email", ``);

    if (user.number === "") {
      inValid("number", `Please fill your phone number`);
    } else inValid("number", ``);

    
    if (user.courses === "Choose") {
      inValid("select-course", `Please choose your course`);
    } else inValid("select-course", ``);

    if (
      user.name !== "" &&
      user.email !== "" &&
      user.number !== "" &&
      emailExactPattern(email) &&
      user.courses !== "Choose"
    )
      return true;
    else return false;


  }



  if(validate()){
      axios
      .post("/coding-apprenticeship-programme/submit", user)
      .then((response) => {
        const addedUser = response.data;
        localStorage.setItem(
          "paymentLinks",
          JSON.stringify({
            status: addedUser.status,
          })
        );
        window.location.pathname = addedUser.path;
      })
      .catch((error) => console.error(error));
  }


  
  function emailExactPattern(element) {
    const regex =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regex.test(element.value)) {
      return true;
    }
  }

  function inValid(element, message) {
    let input = document.getElementsByName(element)[0];
    input.nextElementSibling.innerHTML = message;
    input.nextElementSibling.style.color = "rgb(238, 50, 50)";
  }


}
