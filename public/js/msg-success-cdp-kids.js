function loadSuccessPage(elem) {
  let local = localStorage.getItem("paymentLinks");

  if (!local) {
    window.location.replace(elem.location.origin);
  } else {
    let localData = JSON.parse(local.toString());
    document.getElementsByClassName("body")[0].style.display = "block";

    let cdpRow = document.getElementById("cdp-row");

    switch (localData.courses) {
      case "Cubs":
        document.getElementById("course-p").style.display = "none";
        cdpRow.innerHTML = `
                    <div class="c-card m-3" style="height:350px !important; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); position: relative; border-radius: 4px; border:1px solid #e5e5e5; padding: 10px; width: fit-content;">
                        <div class="c-card" style="height: 100%; width: 290px !important; padding:60px 20px; border-radius: 4px; border:1px solid #e5e5e5; padding-top: 30px; padding-bottom: 30px; width: fit-content;">
                        <p style="color: rgb(40, 119, 40); font-size: 20px; margin-bottom:10px;">Cubs payment plan</p> 
                        <h1 style="font-weight: 600; font-size:50px;">FREE</h1>   
                        <div class="blog-btn">
                            <ul>
                                <li style="font-size:14px; list-style: inherit; margin-left: 15px;">For a 3 month course</li>
                            </ul>
                            <!-- <img src="./img/icon/flutterwave.png" width="150"style="margin-top:10px; margin-bottom: 138px;" alt="flutterwave_logo"/> <br> -->
                            <!-- <a class="mm-btn" style="margin-top: 0 !important; margin-bottom: 0 !important; padding: 10px 15px; border-radius: 5px; color: #ffffff; text-decoration: none; width:100%;" href="https://flutterwave.com/pay/umbjjlt0cbpa">Proceed with the payment</a> -->
                    </div>
        `;

        return;

      case "Junior Cubs":
        cdpRow.innerHTML = `
                    <div class="c-card m-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); border-radius: 4px; border:1px solid #e5e5e5; padding: 10px; width: fit-content;">
                    <div class="c-card" style=" height: 100%; width: 290px !important; padding:60px 20px; border-radius: 4px; border:1px solid #e5e5e5; padding-top: 30px; padding-bottom: 30px; width: fit-content;">
                    <p style="color: rgb(40, 119, 40); font-size: 20px; margin-bottom:10px;">Junior Cubs Payment Plan</p> 
                    <h1 style="font-weight: 600; font-size:50px;">&#8358; 40,000</h1>   
                    <div class="blog-btn">
                        <ul>
                            <li style="font-size:14px; list-style: inherit; margin-left: 15px;">For a 2 month course</li>
                        </ul>
                        <img src="./img/icon/flutterwave.png" width="150"style="margin-top:10px; margin-bottom: 114px;" alt="flutterwave_logo"/> <br>
                        <a class="mm-btn" style="margin-top: 0 !important; margin-bottom: 0 !important; padding: 10px 15px; border-radius: 5px; color: #ffffff; text-decoration: none; width:100%;" href="https://flutterwave.com/pay/xu8smiysr8rz">Proceed with the payment</a>
                </div>
                </div>
            </div>
            `;
        return;
      case "Pride":
        cdpRow.innerHTML = `
            <div class="c-card m-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); border-radius: 4px; border:1px solid #e5e5e5; padding: 10px; width: fit-content;">
            <div class="c-card" style="height: 100%; width: 290px !important;padding:60px 20px; border-radius: 4px; border:1px solid #e5e5e5; padding-top: 30px; padding-bottom: 30px; width: fit-content;">
            <p style="color: rgb(40, 119, 40); font-size: 20px; margin-bottom:10px;">Pride Payment Plan</p> 
            <h1 style="font-weight: 600; font-size:50px;">&#8358; 50,000</h1>   
            <div class="blog-btn">
                <ul>
                    <li style="font-size:14px; list-style: inherit; margin-left: 15px;">For a 2 month course</li>
                </ul>
                <img src="./img/icon/flutterwave.png" width="150"style="margin-top:10px; margin-bottom: 113px;" alt="flutterwave_logo"/> <br>
                <a class="mm-btn" style="margin-top: 0 !important; margin-bottom: 0 !important; padding: 10px 15px; border-radius: 5px; color: #ffffff; text-decoration: none; width:100%;" href="https://flutterwave.com/pay/rnmttoizlp7r">Proceed with the payment</a>
        </div>
        </div>
    </div>
    `;
        return;

      default:
        break;
    }
  }
}
