let aic = document.querySelector("#aic-submit");
aic.addEventListener("click", aicProcessor);

let cdpRadio;

function getRadioValues(tag) {
  cdpRadio = tag.value;
}

function aicProcessor(e) {
  e.preventDefault();
  let firstName = document.querySelector("#aic-firstname");
  let lastName = document.querySelector("#aic-lastname");
  let email = document.querySelector("#aic-email");
  let number = document.querySelector("#aic-number");
  let courses = document.querySelector("#aic-courses");
  let message = document.querySelector("#aic-message");
  let date = new Date();

  let user = {
    firstName: firstName.value,
    lastName: lastName.value,
    email: email.value,
    number: number.value,
    radio: cdpRadio,
    courses: courses.value,
    message: message.value,
    createdAt: `Created at: ${date.getDate()}:${date.getMonth()}:${date.getFullYear()}`
  };


  const validate = () => {
    if (user.firstName === "") {
      inValid("firstName", `Please fill your first name`);
    } else inValid("firstName", ``);

    if (user.lastName === "") {
      inValid("lastName", `Please fill your last name`);
    } else inValid("lastName", ``);

    if (user.email === "" || !emailExactPattern(email)) {
      inValid("email", `Please fill or provide a valid email`);
    } else inValid("email", ``);

    if (user.number === "") {
      inValid("number", `Please fill your phone number`);
    } else inValid("number", ``);

    if (!user.radio) {
      inValid("gender-male", `Please select your gender`);
    } else inValid("gender-male", ``);

    if (user.courses === "Choose") {
      inValid("select-course", `Please choose your course`);
    } else inValid("select-course", ``);




    if (
      user.firstName !== "" &&
      user.lastName !== "" &&
      user.email !== "" &&
      emailExactPattern(email) &&
      user.number !== "" &&
      user.radio &&
      user.courses !== "Choose"
    )
      return true;
    else return false;

  }


  if (validate()) {

    axios
      .post("/adult-it-classes/submit", user)
      .then((response) => {
        const addedUser = response.data;
        localStorage.setItem(
          "paymentLinks",
          JSON.stringify({
            status: addedUser.status,
          })
        );
        window.location.pathname = addedUser.path;
      })
      .catch((error) => console.error(error));
  }

  function emailExactPattern(element) {
    const regex =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regex.test(element.value)) {
      return true;
    }
  }

  function inValid(element, message) {
    let input = document.getElementsByName(element)[0];
    input.nextElementSibling.innerHTML = message;
    input.nextElementSibling.style.color = "rgb(238, 50, 50)";
  }

}
