## Asetech website

![Logo](./img/logo/asetechlogo-preview.png)

## description

This is a website for asetech Academy, Asetech is an academy that teaches cubs-adult, various programming languages. to enable them cultivate a skill that will help them in the labour market or building products that will help others, they also hlp them get oustanding in the tech world.

## Hosted on

Nestify.app

### Motivation

The design and the message that will be passed across.

## code style

Boostrap

### Built with

Html, css, Javascript and boostrap

### Test

- Only functionality testing was done on the site which is the process of intreracting with the site by clicking of button.

### Credits

To the mangement for their support and patience while the project was still in development. To Mr. Fadlu Gaji for support, advice and patience.

- To Mr. Godfrey for leadership support. For not putting pressure on me for content delivery, instructions delivery and taking out time to make corrections.

- And to Miss. Glory for content support.

- Miss Ebere for taking out her time to go through the website, checking errors and checking out the functionality of the website.

**Assigned to: Okene Emnet Chibuzor**
