const request = require("request");
require("dotenv").config();

exports.facebookChatBotPostWebHook = (req, res, next) => {
  let body = req.body;

  // Checks this is an event from a page subscription
  if (body.object === "page") {
    // Iterates over each entry - there may be multiple if batched
    body.entry.forEach(function (entry) {
      // Gets the body of the webhook event
      let webhook_event = entry.messaging[0];
      console.log(webhook_event);

      // Get the sender PSID
      let sender_psid = webhook_event.sender.id;
      console.log("Sender PSID: " + sender_psid);

      // Check if the event is a message or postback and
      // pass the event to the appropriate handler function
      if (webhook_event.message) {
        handleMessage(sender_psid, webhook_event.message);
      } else if (webhook_event.postback) {
        handlePostback(sender_psid, webhook_event.postback);
      }
    });

    // Returns a '200 OK' response to all requests
    res.status(200).send("EVENT_RECEIVED");
  } else {
    // Returns a '404 Not Found' if event is not from a page subscription
    res.sendStatus(404);
  }
};

exports.facebookChatBotGetWebHook = (req, res) => {
  // Your verify token. Should be a random string.
  let VERIFY_TOKEN = process.env.PAGE_ACCESS_TOKEN;

  // Parse the query params
  let mode = req.query["hub.mode"];
  let token = req.query["hub.verify_token"];
  let challenge = req.query["hub.challenge"];

  // Checks if a token and mode is in the query string of the request
  if (mode && token) {
    // Checks the mode and token sent is correct
    if (mode === "subscribe" && token === VERIFY_TOKEN) {
      // Responds with the challenge token from the request
      console.log("WEBHOOK_VERIFIED");
      res.status(200).send(challenge);
    } else {
      // Responds with '403 Forbidden' if verify tokens do not match
      res.sendStatus(403);
    }
  }
};

// Handles messages events
function handleMessage(sender_psid, received_message) {
  let response;

  // Checks if the message contains text
  if (received_message.text) {
    // Create the payload for a basic text message, which
    // will be added to the body of our request to the Send API
    if (received_message.text === "Hello" || received_message.text === "Hi") {
      response = {
        text: `Hi. I'm glad to meet you. I'm AseBot, what can I do for you today?`,
      };
      return;
    }

    if (
      received_message.text.toLowerCase().split(" ").join("") ===
        "goodmorning" ||
      received_message.text.toLowerCase().split(" ").join("") ===
        "goodafternoon" ||
      received_message.text.toLowerCase().split(" ").join("") === "goodevening"
    ) {
      response = {
        text: `${received_message.text}. I'm AseBot, what can I do for you at this moment?`,
      };
      return;
    }

    if (
      received_message.text.toLowerCase().includes("price") ||
      received_message.text.toLowerCase().includes("cost") ||
      received_message.text.toLowerCase().includes("programmes") ||
      (received_message.text.toLowerCase().includes("programme") &&
        received_message.text.toLowerCase().includes("how much")) ||
      (received_message.text.toLowerCase().includes("cost") &&
        received_message.text.toLowerCase().includes("course"))
    ) {
      response = {
        text: `Type "Programme costs" if you want to find out how much the programmese cost?`,
      };

      if (
        received_message.text.toLowerCase().split(" ").join("") ===
        "programmecosts"
      ) {
        response = {
          text: `
                            Register for any of the programmes you wish to take and get the prices for the 
                            programme or the categories of the programme.
                    `,
        };
      }
      return;
    }

    if (
      received_message.text
        .toLowerCase()
        .includes(
          "programmes",
          "programme",
          "program",
          "About",
          "much",
          "know",
          "offer",
          "offering"
        )
    ) {
      response = {
        text: `
                We offer 4 courses which are all IT related the are:

                    Community Development Programme
                    Coding Apprentenship Programme
                    Coding Fundamental Programme
                    General Courses
                

                Every one off these programmes offered to the community under specific age range. Example, the Community Development programme has two categories. One for the children from the age of 5 to 15 years and the Adult IT classes from Adults from the age of 55 years and above. 
                to know more about the programmes, visit courses section at https://asetech-academy.herokuapp.com/courses.
                `,
      };
        return;
    }
    

    
    response = {
        text: `
                Sorry. I don't understand your message. Please try again or contect us to find out more information.
            `,
      };



  } else if (received_message.attachments) {
    // Get the URL of the message attachment
    let attachment_url = received_message.attachments[0].payload.url;
    response = {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [
            {
              title: "Is this the right picture?",
              subtitle: "Tap a button to answer.",
              image_url: attachment_url,
              buttons: [
                {
                  type: "postback",
                  title: "Yes!",
                  payload: "yes",
                },
                {
                  type: "postback",
                  title: "No!",
                  payload: "no",
                },
              ],
            },
          ],
        },
      },
    };
  }

  // Send the response message
  callSendAPI(sender_psid, response);
}

// Handles messaging_postbacks events
const handlePostback = async (sender_psid, received_postback) => {
  let response;

  // Get the payload for the postback
  let payload = received_postback.payload;

  // Set the response based on the postback payload
  switch (payload) {
    case "GET_STARTED":
    case "RESTART_CONVERSATION":
      await chatbotService.sendMessageWelcomeNewUser(sender_psid);
      break;
    case "TALK_AGENT":
      await chatbotService.requestTalkToAgent(sender_psid);
      break;
    default:
      console.log("run default switch case");
  }
  // Send the message to acknowledge the postback
  // callSendAPI(sender_psid, response);
};

// Sends response messages via the Send API
function callSendAPI(sender_psid, response) {
  // Construct the message body
  let request_body = {
    recipient: {
      id: sender_psid,
    },
    message: response,
  };

  // Send the HTTP request to the Messenger Platform
  request(
    {
      uri: "https://graph.facebook.com/v2.6/me/messages",
      qs: { access_token: process.env.PAGE_ACCESS_TOKEN },
      method: "POST",
      json: request_body,
    },
    (err, res, body) => {
      if (!err) {
        console.log("message sent!");
      } else {
        console.error("Unable to send message:" + err);
      }
    }
  );
}

// let mess = "How much does a programme cost?";

// console.log(mess.includes("programme" && "cost"));
