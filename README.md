# Asetech Academy

Live @ [http://asetech.fairexxitsolutions.com/](http://asetech.fairexxitsolutions.com/)

### Description

Asetech Academy is an institution that trains, builds, and equips individuals with the relevant, must have digital skills necessary for this 21st century.
We equip individuals with the unique capability to create and build, tech solutions in every market space and at every age bracket.

## Hosted On

- AWS (Both Client and Server)

### Motivation

Leadership, Zeal and Teamwork.

### Code Style

JavaScript Style.

![Asetech Logo](./public/img/logo/asetechlogo-preview.png)

### Built With

- HTML
- Node Express.js

### Installation

#### Client Side - (React)

Clone this project to your local computer, open a terminal with the path of the folder, open a terminal in your folder, and type the following command in your terminal:

`yarn install`

This will install all the packages integrated to the react library. You need to have node.js installed in your system and yarn in order to use it. For more information on how to install both on your local computer and how to use them, see the [Node.js](https://nodejs.org/en/) and [yarn documention](https://classic.yarnpkg.com/en/docs/install/#windows.-stable) and the on their website for more details.

After that you need to write the following command:

`yarn start`

This will get it running on your system and will create a local http server and automatically open it for you to view in your local browser.

#### Server Side - (Node Express.js)

There is a folder named server located in this repository for the serverside code. The folder functions are separate from the client site code and they run independent from the client side code.

#### Installation - (Server Side)

Open a terminal with the path of the folder named **server** on your local computer and do the same process done to the client side. run `yarn install` to install the necesary packages and then run `node index` to start the server. Note: Node.js must be installed on your computer system to run this set of code.

### Liscence

MIT

### Credits

To the mangement for their support and patience while the project was still in development. To Mr. Fadlu Gaji for support, corrections and advice and patience.

- To Mr. Godfrey for leadership support. For not putting pressure on me for content delivery, instructions delivery and so on.

- And to Miss. Glory for content support, she helped in the restructing of word content on the site as well as giving me update from Bree concerning changes to be made in the site.

**Team: Miracle King Abaye, Okene Emnet**
