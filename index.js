const express = require("express");
const app = express();
const cors = require("cors");
const axios = require("axios");
const { google } = require("googleapis");
const Analytics = require("analytics").default;
const googleAnalytics = require("@analytics/google-analytics").default;
// const customerIo = require('@analytics/customerio');
const msoschools = require("./data/msoschools");
const mailchimp = require("@mailchimp/mailchimp_marketing");
const mailer = require("./utils/mailerFunc");
const { user_message, admin_message } = require("./utils/message");
const {
  facebookChatBotPostWebHook,
  facebookChatBotGetWebHook,
} = require("./controllers/controllers");

require("dotenv").config();
const port = process.env.PORT || 8000;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(__dirname + "/public"));


//The Routes connecting the pages

//Pages
const analytics = Analytics({
  app: "my-app-name",
  version: 100,
  plugins: [
    googleAnalytics({
      trackingId: "UA-121991291",
    }),
    //   customerIo({
    //     siteId: '123-xyz'
    //   })
  ],
});

/* Track a page view */
analytics.page({
  title: "Home",
  href: process.env.TPATH,
  path: "/",
  lol: "Index",
});

app.get("/", async (req, res) => {
  res.status(200).sendFile(`${process.cwd()}/views/index.html`);
});

app.get("/about", (req, res) => {
  res.sendFile(`${process.cwd()}/views/about.html`);
});

app.get("/courses", (req, res) => {
  res.sendFile(`${process.cwd()}/views/courses.html`);
});

app.get("/MSO", (req, res) => {
  res.sendFile(`${process.cwd()}/views/mso.html`);
});

app.get("/contacts", (req, res) => {
  res.sendFile(`${process.cwd()}/views/contact.html`);
});

app.get("/copyright", (req, res) => {
  res.sendFile(`${process.cwd()}/views/copyright.html`);
});

app.get("/community-development-programme", (req, res) => {
  res.sendFile(`${process.cwd()}/views/community-development-program.html`);
});

app.get("/coding-apprenticeship-programme", (req, res) => {
  res.sendFile(`${process.cwd()}/views/coding-apprenticeship-programme.html`);
});

app.get("/coding-fundamental-programme", (req, res) => {
  res.sendFile(`${process.cwd()}/views/coding-fundamental-courses.html`);
});

app.get("/adult-it-classes", (req, res) => {
  res.sendFile(`${process.cwd()}/views/adult-it.html`);
});

app.get("/terms&conditions", (req, res) => {
  res.sendFile(`${process.cwd()}/views/terms&conditions.html`);
});

app.get("/success", (req, res) => {
  res.sendFile(`${process.cwd()}/views/success.html`);
});

app.get("/faq", (req, res) => {
  res.sendFile(`${process.cwd()}/views/faq.html`);
});

app.get("/careers", (req, res) => {
  res.sendFile(`${process.cwd()}/views/careers.html`);
});

app.get("/competition", (req, res) => {
  res.sendFile(`${process.cwd()}/views/competition.html`);
});

app.get("/general-course", (req, res) => {
  res.sendFile(`${process.cwd()}/views/crash-programme.html`);
});

app.get("/bookslot", (req, res) => {
  res.sendFile(`${process.cwd()}/views/asetechcodeclubprogramme.html`);
});


app.get("/mso-inform", (req, res) => {
  res.sendFile(`${process.cwd()}/views/msoInform.html`);
});

//forms
app.get("/community-development-programme-kids-enroll", (req, res) => {
  res.sendFile(`${process.cwd()}/views/community-dev-form.html`);
});

app.get("/adult-it-classes-enroll", (req, res) => {
  res.sendFile(`${process.cwd()}/views/adult-IT-classes.html`);
});

app.get("/adult-it-classes-enroll", (req, res) => {
  res.sendFile(`${process.cwd()}/views/adult-IT-classes.html`);
});

app.get("/coding-apprenticeship-programme-enroll", (req, res) => {
  res.sendFile(`${process.cwd()}/views/coding-apprenticeship-form.html`);
});

app.get("/coding-fundamental-programme-enroll", (req, res) => {
  res.sendFile(`${process.cwd()}/views/coding-fundamental-courses-form.html`);
});

app.get("/general-course-enroll", (req, res) => {
  res.sendFile(`${process.cwd()}/views/crash-programme-form.html`);
});

//SuccessFul Message
app.get("/message-success-adult-IT-classes", (req, res) => {
  res.sendFile(`${process.cwd()}/views/messageSuccess.html`);
});

app.get("/message-success-cdp-kids", (req, res) => {
  res.sendFile(`${process.cwd()}/views/cdp-kids-success.html`);
});

app.get("/message-success-coding-apprenticeship-programme", (req, res) => {
  res.sendFile(`${process.cwd()}/views/cap-success.html`);
});

app.get("/message-success-coding-fundamental-programme", (req, res) => {
  res.sendFile(`${process.cwd()}/views/cfc-success.html`);
});

app.get("/message-success-general-course", (req, res) => {
  res.sendFile(`${process.cwd()}/views/crash-success.html`);
});

app.get("/kodeclubunsuccessful", (req, res) => {
  res.sendFile(`${process.cwd()}/views/kodeclubunregistered.html`);
});


//Extras

app.post("/webhook", facebookChatBotPostWebHook);
app.get("/webhook", facebookChatBotGetWebHook);

const googleSheetProcessor = async (formArray, indent) => {
  const gauth = new google.auth.GoogleAuth({
    keyFile: process.env.GOOGLE_APPLICATION_CREDENTIALS,
    scopes: ["https://www.googleapis.com/auth/spreadsheets"],
  });
  //The id of the sheet
  let spreadsheetId = indent;

  //To get our client
  const client = await gauth.getClient();

  //This google sheet object is used to access all our information.
  const googleSheets = google.sheets({ version: "v4", auth: client });

  const metaData = await googleSheets.spreadsheets.get({
    auth: gauth,
    spreadsheetId: indent,
  });

  // To write to the spreadsheet
  await googleSheets.spreadsheets.values.append({
    auth: gauth,
    spreadsheetId,
    range: "A:B",
    valueInputOption: "USER_ENTERED",
    resource: {
      values: [formArray],
    },
  });

  return metaData.data;
  // }
};

//Object to array function
function formValues(obj) {
  let objToArray = Object.entries(obj).flat(Infinity);

  const getFormValues = objToArray
    .filter((item, index) => {
      if (item === "") item = "null";
      if (index % 2 === 1) {
        return item;
      }
    })
    .map((item) => item);
  return getFormValues;
}

//Route connecting the program forms
app.post("/community-development-programme/submit", async (req, res) => {
  let body = req.body;

  await googleSheetProcessor(formValues(body), process.env.CDPKIDS_ID)
    .then((data) => {
      const responseMessage = `
            <h1>Confirmation Message for Registeration of CDP, Asetech Academy</h1>
            <br/>
            <p style="font-size:14px">Hi ${body.parentName},</p>
            <p style="font-size:14px">
               You have successfully registered for <em><strong>${body.courses}</strong></em> 
               course at <em><strong>Community Development Programme</strong></em> for your child. 
               We are glad you registered with us. We will soon reach out
               to you for the next steps to take in relations to this course. 
            </p>
`;

      mailer
        .mailerFunc(
          process.env.DESTINATION_EMAIL,
          `Registeration Notification for Community Development Programme (Children)`,
          admin_message(
            body.parentName,
            "Community Development Programme",
            "Asetech Community Development Program (kids) Form (Responses)"
          ),
          "Asetech Bot"
        )
        .then(() => {
          mailer.mailerFunc(
            body.email,
            `Registeration Confirmed`,
            responseMessage,
            "Team Asetech"
          );
        })
        .catch((error) => {
          console.log(error);
          return;
        });

      res.json({
        path: "/message-success-cdp-kids",
        status: "access-g",
        course: req.course,
      });
    })
    .catch((err) => res.status(500).json(err));
});

app.post("/adult-it-classes/submit", async (req, res) => {
  let body = req.body;
  await googleSheetProcessor(formValues(body), process.env.CDPADULTS_ID)
    .then(() => {
      mailer
        .mailerFunc(
          process.env.DESTINATION_EMAIL,
          `Registeration Notification for Adult IT Classes`,
          admin_message(
            `${body.firstName} ${body.lastName}`,
            "Adult IT Classes",
            "Asetech Adult IT  Classes"
          ),
          "Asetech Bot"
        )
        .then(() => {
          mailer.mailerFunc(
            body.email,
            `Registeration Confirmed: Adult IT Classes`,
            user_message(
              `${body.firstName} ${body.lastName}`,
              "Adult IT Classes",
              body.courses
            ),
            "Team Asetech"
          );
        })
        .catch((error) => {
          console.log(error);
          res.status(500).json(err);
        });

      res.json({
        path: "/message-success-adult-IT-classes",
        status: "access-g",
      });
    })
    .catch((err) => {
      res.status(500).json(err);
    });
});

app.post("/coding-apprenticeship-programme/submit", async (req, res) => {
  let body = req.body;
  await googleSheetProcessor(formValues(body), process.env.CAP_ID)
    .then(() => {
      mailer
        .mailerFunc(
          process.env.DESTINATION_EMAIL,
          `Registeration Notification for Coding Apprenticeship Programme`,
          admin_message(
            body.name,
            "Coding Apprenticeship Programme",
            "Asetech Coding Apprenticeship program registration form (Responses)"
          ),
          "Asetech Bot"
        )
        .then(() => {
          mailer.mailerFunc(
            body.email,
            `Registeration Confirmed: Coding Apprenticeship Programme`,
            user_message(
              body.name,
              "Coding Apprenticeship Programme",
              body.course
            ),
            "Team Asetech"
          );
        })
        .catch((error) => {
          console.log(error);
          res.status(500).json(err);
        });

      res.json({
        path: "/message-success-coding-apprenticeship-programme",
        status: "access-g",
      });
    })
    .catch((err) => {
      res.status(500).json(err);
    });
});

app.post("/coding-fundamental-programme/submit", async (req, res) => {
  let body = req.body;
  await googleSheetProcessor(formValues(body), process.env.CFC_ID)
    .then(() => {
      mailer
        .mailerFunc(
          process.env.DESTINATION_EMAIL,
          `Registeration Notification for Coding Fundermental Courses`,
          admin_message(
            body.name,
            "Coding Fundermental Programme",
            "Asetech Coding Fundamentals Courses Registration form (Responses)"
          ),
          "Asetech Bot"
        )
        .then(() => {
          mailer.mailerFunc(
            body.email,
            `Registeration Confirmed: Coding Fundermental Courses`,
            user_message(body.name, "Coding Fundermental Courses", body.course),
            "Team Asetech"
          );
        })
        .catch((error) => {
          console.log(error);
          res.status(500).json(err);
        });

      res.json({
        path: "/message-success-coding-fundamental-programme",
        status: "access-g",
      });
    })
    .catch((err) => {
      res.status(500).json(err);
    });
});

//MSO Schools
app.get("/msoschools", (req, res) => {
  res.json({ data: msoschools });
});

app.post("/kodeklub/unregisteredschools", async (req, res) => {
  let body = req.body;
  await googleSheetProcessor(
    formValues(body),
    process.env.INTERESTED_SCHOOLS_ID
  )
    .then(() => {
      const message = `
         <h1>Confirmation Message for Registeration of Asetech KodeKlub Programme</h1>
         <br/>
         <p style="font-size:14px">Hi Team Asetech,</p>
         <p style="font-size:14px">
             Someone has provided an unregistered school to be included in the MSO program.</p>
             The unregistered school is <strong>${body.school}</strong>.
         </p>`;

      mailer.mailerFunc(
        process.env.DESTINATION_EMAIL,
        `Interest in Asetech KodeKlub Programme`,
        message,
        "Asetech Bot"
      );

      res.json({ status: "unmatched", message: "Your message has been sent." });
    })
    .catch((err) => {
      res.status(500).json(err);
    });
});


app.post("/kodeklub/submit", async (req, res) => {
  let body = req.body;
  let check = false;
  //Checking if schools is in the list of schools
  msoschools.schools.forEach((school) => {
    if ((body.school.toLowerCase() === school.name.toLowerCase()) === true) {
      check = true;
      return;
    }
  });

  //check if the school is not found and redirecting it to another page if not found
  if (check === false) {
    res.json({
      path: "/kodeclubunsuccessful",
      status: "error",
    });
    return;
  }

  await googleSheetProcessor(formValues(body), process.env.KODEKLUB_ID)
    .then(() => {
      console.log("admin mail");

      mailer.mailerFunc(
        process.env.DESTINATION_EMAIL,
        `Registeration Notification for Asetech KodeKlub Programme`,
        admin_message(
          body.parentName,
          "Asetech KodeKlub Programme",
          "Asetech Kodeklub Programme"
        ),
        "Asetech Bot"
      );
    })
    .then(() => {
      console.log("user mail");
      const message = `
                  <h1>Confirmation for Booking of Asetech KodeKlub Programme</h1>
                  <br/>
                  <p style="font-size:14px">Hi ${body.parentName},</p>
                  <p style="font-size:14px">
                     You have successfully booked a slot for <em><strong> Asetech KodeKlub Programme </strong></em>. Here are your
                     booking details: 
                     date: ${body.dateSlot} and time: ${body.timeSlot}. 
                  </p>
            `;

      mailer.mailerFunc(
        body.email,
        `Booking Confirmed: Asetech KodeKlub Programme`,
        message,
        "Team Asetech"
      );
    });

  res.json({ status: "unmatched", message: "Your message has been sent." });

  console.log("after success json");
});








app.post("/general-course/submit", async (req, res) => {
  let body = req.body;

  await googleSheetProcessor(formValues(body), process.env.GENERAL_COURSE_ID)
    .then(() => {
      mailer
        .mailerFunc(
          process.env.DESTINATION_EMAIL,
          `Registeration Notification for Coding Fundermental Courses`,
          admin_message(
            body.name,
            "Asetech General Courses",
            "General Courses"
          ),
          "Asetech Bot"
        )
        .then(() => {
          mailer.mailerFunc(
            body.email,
            `Registeration Confirmed: Asetech General Courses`,
            user_message(body.name, "Asetech General Courses", body.course),
            "Team Asetech"
          );
        })
        .catch((error) => {
          console.log(error);
          res.status(500).json(err);
        });

      res.json({
        path: "/message-success-general-course",
        status: "access-g",
        course: req.course,
      });
    })
    .catch((err) => {
      res.status(500).json(err);
    });
});

//Email Suscription
app.post("/suscribe", (req, res) => {
  const { firstName, lastName, email } = req.body;

  if (firstName === "" && lastName === "" && email === "") {
    res.status(400).json({ message: `Please fill the form.` });
    return;
  }

  const listId = process.env.MAILCHIMP_LIST_ID;

  const subscribingUser = {
    firstName,
    lastName,
    email,
  };

  mailchimp.setConfig({
    apiKey: process.env.MAILCHIMP_API_KEY,
    server: process.env.MAILCHIMP_SERVER,
  });

  async function run() {
    const response = await mailchimp.lists
      .addListMember(listId, {
        email_address: subscribingUser.email,
        status: "subscribed",
        merge_fields: {
          FNAME: subscribingUser.firstName,
          LNAME: subscribingUser.lastName,
        },
      })
      .catch((err) => {
        if (err.status === 404) {
          res.status(404).json(res.status, "not found");
          return;
        } else {
          res.status(400).json({ message: err.response.text });
        }
      });
    if (!response) return;
    else res.json({ message: "You have successfully suscribed" });
    return;
  }

  run();
});

app.post("/contact-submit", (req, res) => {
  let body = req.body;

  const message = `
      <p>${body.message}</p>
      <p><strong>${body.firstName} ${body.lastName}</strong></p>
      <p>${body.email}</p>
      `;

  mailer
    .mailerFunc(
      `Message from Asetech's Contact Page by ${body.firstName} ${body.lastName}`,
      message,
      "Asetech Bot"
    )
    .then(() => {
      res.send("Message Sent");
    })
    .catch((err) => {
      res.send(err);
    });
});

//Powering up the server
app.listen(port, () => console.log(`your port is running at ${port}`));
